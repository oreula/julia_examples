### A Pluto.jl notebook ###
# v0.12.20

using Markdown
using InteractiveUtils

# ╔═╡ 85de5514-71fe-11eb-30be-013df5afb2ff
begin
	using DifferentialEquations
	using Plots
end

# ╔═╡ 9a76a276-71fe-11eb-129c-0d6a180f9802
begin
	l = 1.0                             # length [m]
	m = 1.0                             # mass[m]
	g = 9.81                            # gravitational acceleration [m/s²]
	
end

# ╔═╡ 64ef99e6-71fe-11eb-3eeb-4713812977b4
begin
	
	function pendulum!(du,u,p,t)
	    du[1] = u[2]                    # θ'(t) = ω(t)
	    du[2] = -3g/(2l)*sin(u[1]) + 3/(m*l^2)*p(t) # ω'(t) = -3g/(2l) sin θ(t) + 3/(ml^2)M(t)
	end
	
	θ₀ = 0.01                           # initial angular deflection [rad]
	ω₀ = 0.0                            # initial angular velocity [rad/s]
	u₀ = [θ₀, ω₀]                       # initial state vector
	tspan = (0.0,10.0)                  # time interval
	
	M = t->0.1sin(t)                    # external torque [Nm]
	
	prob = ODEProblem(pendulum!,u₀,tspan,M)
	sol = solve(prob)
	
	
end

# ╔═╡ b4d989a8-71fe-11eb-2e7e-7d186324acc2
plot(sol,linewidth=2,xaxis="t",label=["θ [rad]" "ω [rad/s]"],layout=(2,1))

# ╔═╡ e54d8162-7207-11eb-23f6-37352bb1690f
f(x) = sin(x)^2

# ╔═╡ f4343f86-7207-11eb-0e4a-25476b1642d7
plot(f)

# ╔═╡ Cell order:
# ╠═85de5514-71fe-11eb-30be-013df5afb2ff
# ╠═9a76a276-71fe-11eb-129c-0d6a180f9802
# ╠═64ef99e6-71fe-11eb-3eeb-4713812977b4
# ╠═b4d989a8-71fe-11eb-2e7e-7d186324acc2
# ╠═e54d8162-7207-11eb-23f6-37352bb1690f
# ╠═f4343f86-7207-11eb-0e4a-25476b1642d7

{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "1d4841d8-b54c-446b-ade1-a621311bafea",
   "metadata": {
    "tags": []
   },
   "source": [
    "## Método de Newton Raphson en varias dimensiones: \n",
    "\n",
    "\n",
    "### Teoría\n",
    "\n",
    "Veremos como generalizar el método de Newton-Raphson para el caso en que tenemos una asignación contínua entre $R^n \\to R^n$.\n",
    "\n",
    "Es decir, tenemos un conjunto de n funciones $\\{f^i(x^j)\\}, i=1, \\ldots n$ de las n variables $\\{x^j\\}, j=1, \\ldots n$ y queremos encontrar un cero *simultáneo* de todas estas funciones.\n",
    "\n",
    "Recordando el caso unidimensional, veíamos que la idea era usar serie de Taylor para ver que en el caso de un cero en $x_0$ teníamos,\n",
    "\n",
    "$$\n",
    "0 = f(x_0) = f(x) + \\frac{df}{dx}(x)(x_0-x) + \\frac{df^2}{dx^2}(\\zeta)(x_0-x)^2\n",
    "$$\n",
    "\n",
    "Y por lo tanto, \n",
    "\n",
    "$$\n",
    "x_0 \\approx x - \\frac{f(x)}{\\frac{df}{dx}(x)}\n",
    "$$\n",
    "\n",
    "De allí se sigue con la iteración.\n",
    "\n",
    "$$\n",
    "x_{n+1} = x_n - \\frac{f(x_n)}{\\frac{df}{dx}(x_n)}\n",
    "$$\n",
    "\n",
    "Que sucede cuando tenemos transformaciones n-dimensionales?\n",
    "\n",
    "Recordemos que el teorema de Taylor en dimensiones arbitrarias es:\n",
    "\n",
    "$$\n",
    "0 = f^i(x^j_0) = f^i(x^j) + \\frac{\\partial f^i}{\\partial x^j}(x)(x^j_0-x^j) + \\frac{\\partial^2 f^2}{\\partial x^j \\partial x^k}(\\zeta)(x^j_0-x^j)(x^k_0-x^k)\n",
    "$$\n",
    "\n",
    "Despejando, obtenemos, \n",
    "\n",
    "$$\n",
    "x^j_0 \\approx x^j - (\\frac{\\partial f^i}{\\partial x^j}(x))^{-1} f^i(x)\n",
    "$$\n",
    "\n",
    "Vemos entonces que el proceso es muy similar, excepto que ahora, en vez de dividir por la derivada primera, debemos encontrar la inversa de una matríz, el **Jacobiano** de la transformación.  \n",
    "\n",
    "\n",
    "### Ejemplo de minimización\n",
    "\n",
    "Vamos a mostrar un ejemplo de uso de Newton-Raphson en 2 dimensiones. El ejemplo será de minimización. Supondremos que tenemos datos $(x,y)$, vectores de dimensión $N$, y queremos modelar los mismos con una función que llamaremos el *modelo*. Para que el problema sea dos dimensional supondremos que el modelo depende de dos parámetros y el problema será: \n",
    "\n",
    "*dado un modelo $mod(x,p)$ encuentre los parámetros $p=p_0$ que minimizan la distancia entre $y$ y $mod(x,p)$.*\n",
    "\n",
    "En este ejemplo simple supondremos que el dato es cercano a la función \n",
    "\n",
    "$$\n",
    "y \\approx sin(2x),\n",
    "$$\n",
    "\n",
    "que el modelo viene dado por:\n",
    "$$\n",
    "mod(x,p) = p_1 * sin(p_2*x)\n",
    "$$\n",
    "\n",
    "y que la *distancia* con la que mediremos el error en nuestro modelo viene dada por:\n",
    "\n",
    "$$\n",
    "d(p) = ||y - mod(x,p)||^2 = \\sum_{n=1}^{N} (y_n - mod(x_n,p))^2. \n",
    "$$\n",
    "\n",
    "La anterior no es matemáticamente una distancia, debieramos tomar la raiz cuadrada de esa expresión, y también dividir por el número total de puntos, cosa que si agregamos más puntos la misma se mantenga en valores cercanos. Pero el mínimo de esta distancia se corresponde con el mínimo de la función $d(p)$ y en este último caso las cuentas son más simples.\n",
    "\n",
    "Minimizaremos ahora la función $d(p)$, como los datos no serán exáctamente los obtenidos por $y=sin(x)$ nuestro esquema encontrará valores cercanos a $(p_1,p_2) = (1,2)$."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "5854447b-8632-40cd-b944-7471a91af2f1",
   "metadata": {
    "jupyter": {
     "source_hidden": true
    },
    "tags": []
   },
   "outputs": [],
   "source": [
    "using Plots\n",
    "using LinearAlgebra\n",
    "using LaTeXStrings\n",
    "\"\"\"\n",
    "Plots the values of a matrix as a surface plot.\n",
    "\"\"\"\n",
    "function plot_matrix(A::Matrix{Float32}; fc=:ocean, linealpha=0.3, fillalpha=0.5, camera=(60,40), title = \"\")\n",
    "    default(size=(600,600)\n",
    "#, fc=:thermal\n",
    "#, fc=:heat\n",
    "    , fc=fc\n",
    "    )\n",
    "    if !(ndims(A) == 2) \n",
    "        error(\"Array must be 2 dimensional and seems to be of dims = $(ndims(A))\")\n",
    "    end\n",
    "    (n,m) = size(A)\n",
    "    x, y = 1:n, 1:m\n",
    "    z = Surface((x,y)->A[x,y], x, y)\n",
    "    surface(x,y,z, linealpha = linealpha, fillalpha=fillalpha, display_option=Plots.GR.OPTION_SHADED_MESH, camera=camera, title = title)\n",
    "end"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "ea3bcc76-d0fc-42bd-996f-a3bc4201875c",
   "metadata": {},
   "source": [
    "Fabricamos los datos para nuestro ejemplo. Lo hacemos agregando a la función $\\sin(x)$ valores al azar de un tamaño relativamente pequeño. Usted puede cambiar el *intervalo de muestreo*, `dx`, y el valor de tamaño de nuestra *perturbación* o *ruido* `ϵ`. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "5ed0b79d-231b-4d87-bc2e-d2523d09fb4f",
   "metadata": {},
   "outputs": [],
   "source": [
    "dx = 0.1\n",
    "x = 0:dx:2π\n",
    "ϵ = 0.1\n",
    "y = sin.(2x) + ϵ*(rand(length(x)) .- 0.5);\n",
    "scatter(x,y)\n",
    "plot!(x,sin.(2*x))"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "9c97cbe4-afed-4b9d-96fe-e1b9341eb4de",
   "metadata": {},
   "source": [
    "Definimos ahora el modelo. Notemos que hemos puesto un `@.` delante de la función. Esto es debido a que como la usaremos con cantidades vectoriales ($x$), este macro hace que esto sea automático."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "2210182b-61bb-4002-823f-ece0d1df388e",
   "metadata": {},
   "outputs": [],
   "source": [
    "@. mod(x,p) = p[1]*sin(p[2]*x)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "a4578724-814f-4937-93c5-fc050f30f7ce",
   "metadata": {},
   "source": [
    "Definimos ahora la función distancia:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "29ef0589-68eb-45b5-a7fd-d608282d39d7",
   "metadata": {},
   "outputs": [],
   "source": [
    "d(p) = (y - mod(x,p))'*(y - mod(x,p))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "3e35e1e6-6bb1-44ea-b495-624eab0cfe45",
   "metadata": {},
   "outputs": [],
   "source": [
    "d([1;1])"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "4d266a66-3af4-49f8-95fc-0a5df2c9ea1f",
   "metadata": {},
   "source": [
    "La graficamos la función d en una región del espacio de parámetros para tener una idea de que sucederá. Estamos buscando el mínimo absoluto de esta función."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "51268278-685b-495e-9fd3-1f4765a68df3",
   "metadata": {
    "tags": []
   },
   "outputs": [],
   "source": [
    "dp1 = 0.01\n",
    "dp2 = 0.01\n",
    "p1 = -1:dp1:3\n",
    "p2 = 0:dp2:3\n",
    "dd = [Float32(d([i,j])) for i in p1, j in p2];\n",
    "plot_matrix(dd, camera=(-60,40))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "fab7b82d-bc80-48a7-8b79-eab0d1f9ecd3",
   "metadata": {
    "jupyter": {
     "source_hidden": true
    },
    "tags": []
   },
   "outputs": [],
   "source": [
    "contour(-1:dp1:3, 0:dp2:3, (p1,p2) -> Float32(d([p1;p2]))\n",
    "        , fc =:beach\n",
    "        , fill = true\n",
    "        , levels = [0,0.3,0.5,1,2,3,10,20,25,29,30,31,32,35,40,50,60,80,90,100,110,200,300,400,500]\n",
    "        , c = cgrad(:ocean)\n",
    "        , xlabel = L\"p_1\", ylabel = L\"p_2\"\n",
    "        , title = L\"d(p)\"\n",
    ")"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "74c032a3-177c-497f-8d03-0f81a712f027",
   "metadata": {},
   "source": [
    "Para encontrar el mínimo usaremos el hecho que el gradiente de la función se anula en dicho punto, por lo tanto usaremos un algoritmo que busque los ceros del gradiente. El método que usaremos en el de Newton-Raphson para encontrar estos puntos.\n",
    "\n",
    "Vemos en las gráficas que hay puntos donde el gradiente de la función es cero, pero que no son mínimos. Nuestro algoritmo no distinguirá estos puntos de el mínimo absoluto, por lo tanto tendremos que tener cuidado en la elección de los parametros donde comenzaremos a buscar los mínimos.\n",
    "\n",
    "Para aplicar el algoritmo debemos entonces encontrar el gradiente, $\\nabla d$, y luego sus derivadas, que forman una matríz llamada el Jacobiano del gradiente.\n",
    "\n",
    "El gradiente está dado por:\n",
    "$$\n",
    "[\\partial_{p_1} d, \\partial_{p_2} d] = -2[(y-mod)'* \\frac{\\partial mod}{\\partial_{p_1}},\\; (y-mod)'* \\frac{\\partial mod}{\\partial_{p_2}}]  =[-2(y-mod)'*(sin(p_2*x), -2(y-mod)'*p_1*x*cos(p_2*x)]\n",
    "$$\n",
    "\n",
    "La prima ${}^{'}$ denota la transpuesta, es decir, estamos escribiendo la función $d$ como:\n",
    "\n",
    "$$\n",
    "d(p) = (y-mod(x,p))'*(y-mod(x,p)).\n",
    "$$"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "b4978280-03aa-4138-aeb1-99c4d9106845",
   "metadata": {},
   "source": [
    "El Jacobiano está definido por:\n",
    "\n",
    "$$\n",
    "Jd = \\left( \n",
    "    \\begin{array}{cc}\n",
    "    \\partial_{p_1p_1}d & \\partial_{p_1p_2}d \\\\\n",
    "    \\partial_{p_2p_1}d & \\partial_{p_2p_2}d \n",
    "    \\end{array}\n",
    "    \\right)\n",
    "    =\n",
    "    \\left( \n",
    "    \\begin{array}{cc}\n",
    "    -2 (y-mod)'* \\frac{\\partial^2 mod}{\\partial_{p_1^2}} + 2\\frac{\\partial mod}{\\partial_{p_1}}^{'}*\\frac{\\partial mod}{\\partial_{p_1}} & -2 (y-mod)'* \\frac{\\partial^2 mod}{\\partial_{p_1}\\partial_{p_2}} + 2\\frac{\\partial mod}{\\partial_{p_2}}^{'}*\\frac{\\partial mod}{\\partial_{p_1}} \\\\\n",
    "    -2 (y-mod)'* \\frac{\\partial^2 mod}{\\partial_{p_1}\\partial_{p_2}} + 2\\frac{\\partial mod}{\\partial_{p_2}}^{'}*\\frac{\\partial mod}{\\partial_{p_1}} & -2 (y-mod)'* \\frac{\\partial^2 mod}{\\partial_{p_2^2}} + 2\\frac{\\partial mod}{\\partial_{p_2}}^{'}*\\frac{\\partial mod}{\\partial_{p_2}} \n",
    "    \\end{array}\n",
    "    \\right)\n",
    "$$\n"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "f1ecc0a9-a6c9-4ede-b241-4ad7686ed5a6",
   "metadata": {},
   "source": [
    "Implementamos estas funciones:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "f37fe2e3-f411-4453-b82b-c66226f48d34",
   "metadata": {},
   "outputs": [],
   "source": [
    "dmod1(p) = sin.(p[2]*x)\n",
    "dmod2(p) = p[1]*(x.*cos.(p[2]*x))\n",
    "function d1(p) \n",
    "    m = y-mod(x,p)\n",
    "    return [-2m'*dmod1(p); -2*m'*dmod2(p)]\n",
    "end"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "5e793a0f-f4d9-4375-915a-ed650040b896",
   "metadata": {},
   "outputs": [],
   "source": [
    "d1([1;1])"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "5222fd7a-ab39-45ca-b027-39cb7b7934cb",
   "metadata": {},
   "outputs": [],
   "source": [
    "d2mod11(p) = 0\n",
    "d2mod12(p) = x.*cos.(p[2]*x)\n",
    "d2mod22(p) = -p[1]*(x.*x.*sin.(p[2]*x))\n",
    "\n",
    "function d2(p) \n",
    "    m = y-mod(x,p)\n",
    "    [2 * dmod1(p)'* dmod1(p)                0 - 2* m' * d2mod12(p) + 2 * dmod2(p)'* dmod1(p) ;\n",
    "     - 2* m' * d2mod12(p) + 2 * dmod2(p)'* dmod1(p)  2*m' * d2mod22(p) + 2 * dmod2(p)'* dmod2(p)]\n",
    "end"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "108e1480-9204-4a7c-a71f-4a19af6525e7",
   "metadata": {},
   "source": [
    "Ahora aplicamos el método de Newton-Raphson para el caso de varias dimensiones.\n",
    "Recordemos que el algoritmo es:\n",
    "\n",
    "$$\n",
    "x = x - (Jf)^{-1}(x)f(x)\n",
    "$$\n",
    "\n",
    "Donde $f:R^n \\to R^n$ (o sea es un vector de componentes $f^i$) y $Jf(x)^i{}_j = \\frac{\\partial f^i}{\\partial x^j}$\n",
    "es su Jacobiano."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "48bc7886-d75c-4058-9eb2-7cb5e3799e7b",
   "metadata": {},
   "outputs": [],
   "source": [
    "p=[0.9;2.1] # frecuencia 2, da bien \n",
    "#p=[0.9;2.2] # frecuencia 2,  no aproxima a la solución\n",
    "\n",
    "for n in 1:5\n",
    "    p = p - d2(p)'\\d1(p)\n",
    "    println(\"p = [$(p[1]); $(p[2])]\")\n",
    "    println(\"d1 = [$(d1(p)[1]); $(d1(p)[2])]\")\n",
    "    println(\"d = $(d(p))\")\n",
    "end"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "c1f5ac9d-6581-4faa-9ba8-4f5a55043a81",
   "metadata": {},
   "outputs": [],
   "source": [
    "scatter(x,y)\n",
    "plot!(x,mod(x,p)) "
   ]
  },
  {
   "cell_type": "markdown",
   "id": "0a03fa7d-d937-48a3-92b8-750b2aa79293",
   "metadata": {},
   "source": [
    "## El paquete LsqFit\n",
    "\n",
    "El tratamiento anterior, con el método de Newton-Raphson, no es la manera usual de encontrar los mínimos de funciones. Para esta tarea hay métodos más específicos y de mucho mejor rendimiento y robustez. \n",
    "\n",
    "En la práctica podemos usar un paquete o librería que incorpora métodos más poderosos. Aquí usaremos el paquete LsqFit de Julia. Lo empleamos en el ejemplo anterior. Note que en este caso no debemos encontrar el Jacobiano no hacer ninguna derivada. El método usado aproxima las derivadas necesarias según necesite."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "42bd3570-04da-4e54-971a-e3e23b9af389",
   "metadata": {},
   "outputs": [],
   "source": [
    "using LsqFit"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "19608363-4d85-4c85-8e46-9ebf8155e179",
   "metadata": {},
   "outputs": [],
   "source": [
    "p0 = [0.9;2.1]\n",
    "fit = curve_fit(mod, x, y, p0);"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "559287c2-59b3-48ae-8e8c-ee190e4d4cfe",
   "metadata": {},
   "outputs": [],
   "source": [
    "fit.param"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "b28654a0-61ee-4357-8552-35a34a7a4675",
   "metadata": {},
   "outputs": [],
   "source": [
    "cov = estimate_covar(fit)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "5ac99bd0-a52e-490c-8415-dfe482f5eacc",
   "metadata": {},
   "outputs": [],
   "source": [
    "se = stderror(fit)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "fc2cc48c-18e3-4cf6-a9fb-cbf7ae6b9e7a",
   "metadata": {},
   "outputs": [],
   "source": [
    "margin_of_error = margin_error(fit, 0.1)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "97c2d42d-3360-493a-9790-221984d2e46b",
   "metadata": {},
   "source": [
    "### Tareas:\n",
    "\n",
    "1. Haga la cuenta para el ajuste lineal, o sea, ajuste datos para modelos de la forma $m(x,p) = p[1] + p[2]*x$. En ese caso para encontrar el mínimo no se necesita resolver más que la inversión de una matríz muy simple. \n",
    "\n",
    "2. Usando el paquete `LsqFit` genere un dato *cercano* a `y = p[1]*cos(p[2]*x) + p[2]*cos(p[1]*x)`, con 2 parámetros, para los valores $p= (1,2)$ encuentre el mejor ajuste. Previamente grafique la distancia en función de los parámetros, verá que existen muchos mínimos y por lo tanto muestra la limitación de estos métodos: será difícil encontrar el mínimo que esperábamos."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "9cfe6217-75a0-4ba3-82a2-8a2f17828796",
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Julia 1.7.2",
   "language": "julia",
   "name": "julia-1.7"
  },
  "language_info": {
   "file_extension": ".jl",
   "mimetype": "application/julia",
   "name": "julia",
   "version": "1.7.2"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}

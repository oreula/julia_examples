### A Pluto.jl notebook ###
# v0.14.4

using Markdown
using InteractiveUtils

# ╔═╡ 9a2cfafe-e0b4-47d2-9331-db54f7e65b10
using Plots

# ╔═╡ 1330966a-a89e-11eb-0912-63348f59a6ec
begin
	N=2000
	M=200
	x=zeros(M,N)
	r=zeros(M)
	x0 = 0.5
	r0 = 0.
	r1 = 2.
	dr = (r1 - r0)/M
	for i ∈ 1:M
	    r[i] = r0 + (i-1)*dr
	    x[i,1] = x0
	    for j ∈ 1:N-1
	        x[i,j+1] = r[i]*x[i,j]*(x[i,j]-1)
	    end
	end
	
end

# ╔═╡ f7e7d164-169c-4198-8f5d-570f5b868ee7
begin
	p = scatter(r[:],x[:,200],
	            markersize = 0.2,
	            markeralpha = 0.8,
	            markercolor = :green,
	            label = false)
	for j ∈ 201:N
	    scatter!(r[:],x[:,j],
	            markersize = 0.2,
	            markeralpha = 0.8,
	            markercolor = :green,
	            label = false)
	end
	display(p)
end

# ╔═╡ ed006169-b3ef-473f-9eb4-9c10fd9f6635
show(p)

# ╔═╡ Cell order:
# ╠═1330966a-a89e-11eb-0912-63348f59a6ec
# ╠═9a2cfafe-e0b4-47d2-9331-db54f7e65b10
# ╠═f7e7d164-169c-4198-8f5d-570f5b868ee7
# ╠═ed006169-b3ef-473f-9eb4-9c10fd9f6635

"""
bisection(f::Function, 
  a::Float64, 
  b::Float64;
  tol_x::Float64=1e-5, 
  tol_y::Float64=1e-7, 
  maxiter::Int64=100)

nos calcula las raices de la función `f` en el intervalo `[a,b]`

retorna un vector con `[raiz, error_x_relativo, error_y, numero_iteraciones]`

# Examples
```julia-repl
julia> 


bisection(mi_f,0.2,3.0;tol_x=1e-10)

4-element Array{Float64,1}:
  1.4142135624540966
  5.762262874758327e-11
  2.2910695562927685e-10
 34.0
```
"""
function bisection(f::Function, 
  a::Float64, 
  b::Float64;
  tol_x::Float64=1e-5, 
  tol_y::Float64=1e-7, 
  maxiter::Int64=100)

  c = (a+b) / 2.0
  fa = f(a)
  fc = f(c)
  if fa*f(b) > 0 
        error("la función tiene el mismo signo en ambos extremos de [$a,$b]")
    end

  i = 0
      while abs(b-a)/abs(b+a) > tol_x || abs(fc) > tol_y

          i += 1 # i = i + 1
          if i > maxiter 
             error("Numero maximo de iteraciones excedido")
          end

          c = (a+b) / 2.0
          fc = f(c)

          if fc == 0
              break
          elseif fa*fc > 0
              a = c  # La raiz esta en la mitad derecha de [a,b].
              fa = fc
          else
              b = c  # La raiz esta en la mitad izquierda de [a,b].
          end

    end #while

  return [c, abs(b-a)/abs(b+a),abs(fc),  i]
end